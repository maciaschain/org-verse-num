;;; ;;; org-verse-num.el --- Displays verse numbering in the margin of an Org `verse' block  -*- lexical-binding: t -*-

;; Copyright (C) 2020-2022  Juan Manuel Macías

;; Author: Juan Manuel Macías <maciaschain@posteo.net>
;; URL: https://gitlab.com/maciaschain/org-verse-nums
;; Version: 0.9
;; Package-Requires: ((emacs "27.2") (org "9.4"))
;; Keywords: org-mode, poetry, verse, verse numbering

;; This file is not part of GNU Emacs.

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 2, or (at your option)
;; any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program; see the file COPYING.  If not, write to the
;; Free Software Foundation, Inc., 59 Temple Place - Suite 330,
;; Boston, MA 02111-1307, USA.

;;; Code:

(provide 'org-verse-num)

;;;###autoload
(defun org-verse-num (&optional num)
  "Displays verse numbering in the margin of an Org `verse' block.

By default, the numbering sequence is 5, unless indicated by the
optional argument NUM.  The function must be evaluated within a
`verse' block."
  (if num
      (setq vnum-advance num)
    (setq vnum-advance 5))
  (let
      ((n vnum-advance))
    (setq left-margin-width 4)
    (set-window-buffer (selected-window) (current-buffer))
    (save-excursion
      (save-restriction
	(org-narrow-to-block)
	(goto-char (point-min))
	(re-search-forward "#\\+begin_verse\\|#\\+BEGIN_VERSE" nil t)
	(let
	    ((versenum 0))
	  (save-excursion
	    (condition-case nil
		(while
		    (re-search-forward "^.+" nil nil n)
		  (unless (looking-back "#\\+end_verse\\|#\\+END_VERSE")
		    (let
			((ov (make-overlay (point) (point))))
		      (overlay-put ov 'overlay-num-verse t)
		      (overlay-put ov 'before-string
				   (propertize " "
					       'display
					       `((margin left-margin)
						 ,(propertize
						   (format
						    "%s"
						    (number-to-string
						     (setf versenum
							   (+ versenum
							      (or num 5)))))
						   'face 'linum)))))))
	      (error (ignore)))))))))

;;;###autoload
(defun org-verse-num-show ()
  (interactive)
  (if (not (equal (org-element-type (org-element-at-point)) 'verse-block))
      (error "Not in a verse block...")
    (if (equal current-prefix-arg nil) ; no C-u
	(org-verse-num)
      (let
	  ((seq (read-from-minibuffer "Sequence: ")))
	(org-verse-num (string-to-number seq))))))

;;;###autoload
(defun org-verse-num-hide ()
  (interactive)
  (remove-overlays nil nil 'overlay-num-verse t)
  (setq left-margin-width 0)
  (set-window-buffer (selected-window) (current-buffer)))

;;;###autoload
(defun org-verse-num-locate-verse ()
  (interactive)
  (save-restriction
    (org-narrow-to-block)
    (let
	((num (read-number "Go to verse...")))
      (goto-char (point-min))
      (re-search-forward "#\\+begin_verse\\|#\\+BEGIN_VERSE" nil t)
      (dotimes (x num)
	(re-search-forward "^." nil t))
      (beginning-of-line))))

;;;###autoload
(defun org-verse-num-act ()
  (if (not (equal (org-element-type (org-element-at-point)) 'verse-block))
      (error "Not in a verse block...")
    (org-verse-num-hide)
    (org-verse-num)))

;;;###autoload
(define-minor-mode org-verse-num-mode
  "Displays and updates verse numbers in a `verse' block.

This is a minor mode.  If called interactively, toggle the
‘org-verse-num mode’ mode."
  :init-value nil
  :lighter ("verses")
  (if org-verse-num-mode
      (add-hook 'post-self-insert-hook #'org-verse-num-act nil 'local)
    (remove-hook 'post-self-insert-hook #'org-verse-num-act 'local)
    (org-verse-num-hide)))

;;; org-verse-num.el ends here
